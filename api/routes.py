"""Web application api routes"""
from aiohttp import web

from . import views

ROUTES = [
    web.get('/users/', views.UserView),
    web.post('/users/', views.UserView),
    web.put('/users/{user_id}/', views.UserView),
    web.delete('/users/', views.UserView),
]
