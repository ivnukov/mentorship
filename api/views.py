"""
REST API web handlers
"""
from http import HTTPStatus

from aiohttp import web

from core.decorators import auth_required
from core.models import SQLiteModel


class UserView(web.View):
    """
    Web interface to perform operations with users table
    """
    model = SQLiteModel('users')

    @auth_required()
    async def get(self):
        """
        Handle GET requests
        :return: HTTP Response with user data
        """
        user_id = self.request.query.get('user_id')
        if user_id is None:
            raise web.HTTPBadRequest
        response = await self.model.read(id=user_id)
        return web.json_response(data=response, status=HTTPStatus.OK)

    @auth_required()
    async def post(self):
        """
        Handle POST requests
        :return: HTTP Response with created entry data
        """
        data = await self.request.json()
        if not all([data.get('username'), data.get('password')]):
            raise web.HTTPBadRequest
        response = await self.model.create(data=data)
        return web.json_response(data=response, status=HTTPStatus.CREATED)

    @auth_required()
    async def put(self):
        """
        Handle PUT requests
        :return: HTTP Response with updated data
        """
        data = await self.request.json()
        user_id = self.request.match_info.get('user_id')
        if user_id is None:
            raise web.HTTPBadRequest
        response = await self.model.update(obj_id=user_id, data=data)
        return web.json_response(response, status=HTTPStatus.OK)

    @auth_required()
    async def delete(self):
        """
        Handle DELETE requests
        :return: Empty response (204)
        """
        user_id = self.request.query.get('user_id')
        if user_id is None:
            raise web.HTTPBadRequest
        await self.model.delete(id=user_id)
        return web.json_response(status=HTTPStatus.NO_CONTENT)
