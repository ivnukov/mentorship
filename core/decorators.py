""" Decorators will live here """

from functools import wraps

from aiohttp import web

from core.exceptions import SQLiteException


def auth_required():
    """ Check authorization header"""

    def decorator(func):
        @wraps(func)
        async def wrapper(endpoint):
            # check if endpoint is Class Based View or common function
            headers = endpoint.request.headers if isinstance(endpoint, web.View) \
                else endpoint.headers
            if 'Authorization' not in headers:
                return web.json_response(status=401, data='Unauthorized')
            response = await func(endpoint)
            return response

        return wrapper

    return decorator


def sqlite_method_decorator():
    """Wraps required methods of sqlite models to raise custom exception"""
    def decorator(func):
        @wraps(func)
        async def wrap(cls, query, *args, **kwargs):
            try:
                return await func(cls, query, *args, **kwargs)
            except Exception as err:
                raise SQLiteException(
                    message=err.__str__(),
                    query=query
                )

        return wrap

    return decorator
