"""
Module for custom exceptions
"""


class SQLiteException(Exception):
    """
    Custom exception for handling sqlite operations
    """

    def __init__(self, message, query):
        super(SQLiteException, self).__init__()
        self.message = message
        self.query = query

    def __str__(self):
        return f"{self.message}: {self.query}"
