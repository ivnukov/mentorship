"""Base models description"""
import uuid
from abc import ABC, abstractmethod

import sqlite3

import settings
from core.decorators import sqlite_method_decorator


class ABSModel(ABC):
    """Abstract base class for models"""

    @abstractmethod
    async def run_query(self, query, *args, **kwargs):
        """
        Execute SQL query
        :param query: query to execute
        :param args: additional parameters
        :param kwargs: additional parameters
        :return: query execution result
        """

    @abstractmethod
    async def fetch_from_db(self, query, *args, **kwargs):
        """
        Read and transform data from DB to python
        :param query: query to execute
        :param args:  additional parameters
        :param kwargs:  additional parameters
        :return: data from database
        """

    @abstractmethod
    async def create(self, data, *args, **kwargs):
        """
        Create record to database
        :param data: data to write to database
        :param args: additional parameters
        :param kwargs: additional parameters
        :return: result of query execution
        """

    @abstractmethod
    async def read(self, *args, **kwargs):
        """
        Read record from database
        :param args: additional parameters
        :param kwargs: additional parameters
        :return: result of query execution
        """

    @abstractmethod
    async def update(self, obj_id, data, *args, **kwargs):
        """
        Update record in database
        :param obj_id: object unique id to update
        :param data: data to write into database
        :param args: additional parameters
        :param kwargs: additional parameters
        :return: result of query execution
        """

    @abstractmethod
    async def delete(self, *args, **kwargs):
        """
        Delete record from database
        :param args: additional parameters
        :param kwargs: additional parameters
        :return: result of query execution
        """


class SQLiteModel(ABSModel):
    """
    Class to perform operations with SQLite Database
    """

    def __init__(self, table_name):
        """
        Assign table to class
        :param table_name: table name to perform operations
        """
        self.table_name = table_name

    @staticmethod
    async def generate_unique_id():
        """Generate random uuid"""
        return str(uuid.uuid4()).replace('-', '')

    @sqlite_method_decorator()
    async def run_query(self, query, *args, **kwargs):
        connection = sqlite3.connect(settings.DB_NAME)
        cursor = connection.cursor()
        cursor.execute(query)
        connection.commit()
        connection.close()
        return cursor

    @sqlite_method_decorator()
    async def fetch_from_db(self, query, *args, **kwargs):
        connection = sqlite3.connect(settings.DB_NAME)
        cursor = connection.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        connection.commit()
        connection.close()
        return result

    async def get_ordered_fields(self):
        """Get fields in correct order to create correct query"""
        query = f"SELECT * from {self.table_name}"
        result = await self.run_query(query)
        fields = result.description
        return [x[0] for x in fields]

    async def create(self, data, *args, **kwargs):
        unique_id = await self.generate_unique_id()
        data.update(dict(id=unique_id))
        fields = await self.get_ordered_fields()
        values = [f"'{data.get(x)}'" for x in fields]
        query = f'''INSERT INTO {self.table_name}
                    VALUES ({", ".join(values)})
                '''
        await self.run_query(query)
        return data

    async def update(self, obj_id, data, *args, **kwargs):
        print('hi')
        query = f'''UPDATE {self.table_name}
                    SET {', '.join([f"{x} = '{y}'" for x, y in data.items()])}
                    WHERE id = '{obj_id}'
                    '''
        await self.run_query(query)
        return await self.read(id=obj_id)

    async def delete(self, *args, **kwargs):
        query = f'''DELETE from {self.table_name}
                    WHERE {next(iter(kwargs))} = '{next(iter(kwargs.values()))}'
                  '''
        await self.run_query(query)
        return

    async def read(self, *args, **kwargs):
        fields = await self.get_ordered_fields()
        query = f'''SELECT * from {self.table_name} 3128371932719837198273
                    WHERE {next(iter(kwargs))} = '{next(iter(kwargs.values()))}'
                '''
        fetch_result = await self.fetch_from_db(query)
        result = dict(zip(fields, fetch_result[0]))
        return result
