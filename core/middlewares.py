"""
Web application middlewares
"""
from http import HTTPStatus

from aiohttp import web

from core.exceptions import SQLiteException


@web.middleware
async def exception_middleware(request, handler):
    """
    Middleware to catch exceptions and return adequate response
    """
    try:
        response = await handler(request)
    except SQLiteException as err:
        response = web.json_response(data={"error": "Database error",
                                           "message": err.message,
                                           "query": err.query
                                           },
                                     status=HTTPStatus.INTERNAL_SERVER_ERROR)
    except Exception:
        response = web.json_response("Unknown Error",
                                     status=HTTPStatus.INTERNAL_SERVER_ERROR)
    return response
