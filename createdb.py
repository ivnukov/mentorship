"""Script to create basic tables in database"""
import sqlite3
import uuid

import settings


def run():
    """Create user table in database"""
    conn = sqlite3.connect(settings.DB_NAME)
    cursor = conn.cursor()

    cursor.execute(
        '''CREATE TABLE users (id text PRIMARY KEY, username text UNIQUE , password text)''')
    cursor.execute(f'''INSERT INTO users VALUES
                       ('{str(uuid.uuid4()).replace('-', '')}','admin', 'password')''')

    conn.commit()
    conn.close()


if __name__ == '__main__':
    run()
