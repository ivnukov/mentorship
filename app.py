"""Web application launcher"""
from aiohttp import web

from api import routes
from core.middlewares import exception_middleware


def start_server():
    """Build and start web server"""
    app = web.Application(middlewares=[exception_middleware])
    app.add_routes(routes.ROUTES)

    web.run_app(app)


if __name__ == '__main__':
    start_server()
